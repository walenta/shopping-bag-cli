import { getGlobals } from 'common-es';
import { sep } from 'path';
import { existsSync, copyFileSync, writeFileSync, readFileSync } from 'fs';
import { templates, customersTemplates } from './template-list.js';
import { isCancel } from '@clack/core';
import { cancel } from '@clack/prompts';

export const { __dirname } = getGlobals(import.meta.url);

/**
 * @returns path to root of the project - wher src/ folder is located
 */
export function projectRoot() {
  let resultPath = process.cwd();
  while (!existsSync(`${resultPath}${sep}src`)) {
    const pathCunks = resultPath.split(sep);
    resultPath = pathCunks.slice(0, pathCunks.length - 1).join(sep);

    if (resultPath === '') {
      throw new Error('Project root not found')
    }
  }

  return resultPath;
}

/**
 * 
 * @param {string} replaceIn 
 * @param {number} index 
 * @param {string} replacement 
 * @returns string
 */
export const replaceAt = (replaceIn, index, replacement) => replaceIn.substring(0, index) + replacement + replaceIn.substring(index + replacement.length);

/**
 * 
 * @param {boolean} isCustomersTemplate 
 * @returns Array
 */
export function getTemplateOptions(isCustomersTemplate) {
  return (isCustomersTemplate ? customersTemplates : templates).map(tmpType => ({
    value: tmpType
  }));
}

/**
 * @param {string} chosenTemplate 
 */
export const getScriptName = chosenTemplate => chosenTemplate.split(/[\-\_\.]/).map(s => replaceAt(s, 0, s.at(0).toUpperCase())).join('');

/**
 * @param {string} chosenTemplate 
 */
export function getTemplateName(chosenTemplate) {
  let templateName = chosenTemplate.replace(/[\-\_\.]/g, ' ');
  templateName = replaceAt(templateName, 0, templateName.at(0).toUpperCase());
  return templateName;
}

/**
 * 
 * @param {string} from 
 * @param {string} to 
 * @param {string} file - name of the old file
 * @param {string} chosenTemplate - name of the new file
 * @param {'template' | 'section' | 'snippet'} templateType 
 */
export function copyTemplateFile(from, to, file, chosenTemplate, templateType) {
  copyFileSync(`${from}${sep}${file}`, `${to}${sep}${file.replace(templateType === 'template' ? 'tmp' : templateType, chosenTemplate)}`);
}

export function constructTemplate(destPath, file, chosenTemplate, scriptName, templateName) {
  const filePath = `${destPath}${sep}${file}`;
  let fileContent = readFileSync(filePath, 'utf8');
  fileContent = fileContent.replace(/{{file_name}}/g, chosenTemplate);
  fileContent = fileContent.replace(/{{script_name}}/g, scriptName);
  fileContent = fileContent.replace(/{{template_name}}/g, templateName);

  writeFileSync(filePath, fileContent, 'utf8');
}

export function checkCancel(value, forceCancel) {
  if (forceCancel || isCancel(value)) {
    cancel('Goodbye :)');
    process.exit(0);
  }
}