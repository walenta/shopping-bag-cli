import { select, text } from '@clack/prompts';
import { getTemplateOptions, __dirname, copyTemplateFile, constructTemplate, getTemplateName, getScriptName, projectRoot, checkCancel } from './utils.js';
import { readdirSync, existsSync, mkdirSync } from 'fs';
import { sep } from 'path'

let  templatesFolder = '';

/**
 * 
 * @param {'section' | 'snippet'} type 
 */
export default async function (type) {
  templatesFolder = `${projectRoot()}${sep}src${sep}templates`;
  let isCustomersLocation = false;

  let location = await select({
    message: `Chose location of ${type}`,
    options: [
      { value: 'shared' },
      ...getTemplateOptions(false).filter(temp => templateExists(temp.value, false))
    ]
  });

  checkCancel(location);

  if (location === 'customer') {
    isCustomersLocation = true;
    location = await select({
      message: 'Chose location',
      options: [
        { value: 'shared' },
        ...getTemplateOptions(true).filter(temp => templateExists(temp.value, true))
      ]
    });

    checkCancel(location);
  }

  let name = (await text({
    message: `Enter name for ${type}.`,
    validate: (value) => {
      if (!value.length) {
        return `Please enter name for ${type}.`;
      }

      if (value === 'sections' || value === 'snippets') {
        return 'Name can not be \'sections\' nor \'snippets\''
      }

      const firstChar = value.at(0);
      if (!value.match(/[a-zA-Z0-9-_]+/) || !isNaN(parseInt(firstChar)) || firstChar === '-' || firstChar === '_' || value.includes(' ')) {
        return 'Name can contain only letters, numbers, \'-\' and \'_\'. Name can not start with a number.'
      }
    }
  }));

  checkCancel(name);
  name = name.toLowerCase();

  const destPath = `${getDestPath(location, isCustomersLocation, type)}${sep}${name}`;
  const templateFilesPath = `${__dirname}${sep}assets${sep}${type}`;

  mkdirSync(destPath, { recursive: true });

  readdirSync(templateFilesPath).forEach(file => {
    console.log(`Creating ${destPath}${sep}${file}`);
    copyTemplateFile(templateFilesPath, destPath, file, name, type);
  });

  readdirSync(destPath).forEach(file => {
    constructTemplate(destPath, file, name, getScriptName(name), getTemplateName(name));
  });
}

/**
 * @param {string} template 
 * @param {boolean} isCustomersLocation 
 */
const templateExists = (template, isCustomersLocation) => existsSync(`${templatesFolder}${sep}${isCustomersLocation ? `customers${sep}` : ''}${template}`);

/**
 * @param {string} location 
 * @param {boolean} isCustomersLocation 
 * @param {'section' | 'snippet'} templateType 
 */
function getDestPath(location, isCustomersLocation, templateType) {
  if (location === 'shared') {
    return `${projectRoot()}${sep}src${sep}shared${sep}${templateType}s`;
  }

  return `${templatesFolder}${sep}${isCustomersLocation ? `customers${sep}` : ''}${location}${sep}${templateType}s`;
}
