export const templates = [
  'customers',
  '404',
  'article',
  'blog',
  'cart',
  'collection',
  'gift_card',
  'index',
  'list-collections',
  'page',
  'product',
  'search',
];

export const customersTemplates = [
  'account',
  'activate_account',
  'addresses',
  'login',
  'order',
  'register',
  'reset_password',
];