import { intro, select } from '@clack/prompts';
import color from 'picocolors';
import actionGenerate from './action-generate-controller.js';
import actionProjectInit from './action-project-init.js';
import { checkCancel } from './utils.js';

export async function main() {
  console.clear();

  intro(`${color.bgCyan(color.black(' Shopping bag CLI '))}`);

  const actionType = await select({
    message: 'What would you like to do?',
    options: [
      {
        value: 'generate', label: 'Generate...'
      },
      {
        value: 'project_init', label: 'Initialize new project'
      }
    ]
  })

  checkCancel(actionType);

  switch (actionType) {
    case 'generate':
      const genType = await select({
        message: 'what you want to generate?',
        options: [
          {
            value: 'template', label: 'Template'
          },
          {
            value: 'section', label: 'Section'
          },
          {
            value: 'snippet', label: 'Snippet'
          },
        ]
      })

      checkCancel(genType);
      actionGenerate(genType);
      break;

    case 'project_init':
      actionProjectInit();
      break;
  }
}