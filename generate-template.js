import {select, text} from '@clack/prompts';
import { templates, customersTemplates } from './template-list.js';
import { projectRoot, getTemplateOptions, getScriptName, getTemplateName, __dirname, constructTemplate, copyTemplateFile, checkCancel } from './utils.js';
import { readdirSync, mkdirSync, existsSync } from 'fs';
import { sep } from 'path';

export default async function () {
  let isCustomerTemplate = false;
  let generateAll = false
  let template = await select({
    message: 'What template do you want to generate?',
    options: [
      { value: 'all' },
      ...getTemplateOptions(false)
    ]
  });

  checkCancel(template);

  switch (template) {
    case 'customers':
      isCustomerTemplate = true;
      template = await select({
        message: 'Select cusommer template',
        options: [
          { value: 'all' },
          ...getTemplateOptions(true)
        ]
      })

      checkCancel(template);


      if (template === 'all') {
        generateAll = true;
      }
      break;

    case 'all':
      generateAll = true;
      break;
  }

  if (generateAll) {
    const templateList = isCustomerTemplate ? customersTemplates : templates;
    templateList.filter(t !== 'customers').forEach(t => {
      generateTemplate(t, isCustomerTemplate);
    });
  } else {
    generateTemplate(template, isCustomerTemplate);
  }
}

/**
 * 
 * @param {string} chosenTemplate 
 * @param {boolean} isCustommersTemplate 
 */
async function generateTemplate(chosenTemplate, isCustomersTemplate) {
  let destPath = `${projectRoot()}${sep}src${sep}templates${sep}${isCustomersTemplate ? `customers${sep}` : ''}${chosenTemplate}`;
  let variant = null;

  if (existsSync(destPath)) {
    variant = await group(
      {
        name: () => text({
          message: `Enter name for ${chosenTemplate} template variant.`,
          validate: (value) => {
            if (!value.length) {
              return 'Please enter name for template variant.'
            }

            if (value === 'sections' || value === 'snippets') {
              return 'Name can not be \'sections\' nor \'snippets\''
            }

            const firstChar = value.at(0);
            if (!value.match(/[a-zA-Z0-9-_]+/) || !isNaN(parseInt(firstChar)) || firstChar === '-' || firstChar === '_' || value.includes(' ')) {
              return 'Name can contain only letters, numbers, \'-\' and \'_\'. Name can not start with a number.'
            }
          }
        }),
        isJsonOnly: () => select({
          message: 'Create JSON only variant? (This will inherit liquid markup, CSS and JS)',
          options: [
            {
              value: true, label: 'Yes'
            },
            {
              value: false, label: 'No'
            }
          ]
        })
      },
      {
        oncCacel: () => checkCancel(null, true)
      }
    );

    if (!variant.isJsonOnly) {
      destPath = `${destPath}${sep}${variant.name}`;
    }

    chosenTemplate = `${chosenTemplate}.${variant.name}`;
  }

  const templateFilesPath = `${__dirname}${sep}assets${sep}template`;
  const scriptName = getScriptName(chosenTemplate);
  let templateName = getTemplateName(chosenTemplate);

  mkdirSync(destPath, { recursive: true });

  if (variant?.isJsonOnly) {
    const file = 'tmp.json';
    copyTemplateFile(templateFilesPath, destPath, file, chosenTemplate, 'template');
    constructTemplate(destPath, `${chosenTemplate}.json`, chosenTemplate, scriptName, templateName);
    return;
  }

  readdirSync(templateFilesPath).forEach(file => {
    if (chosenTemplate === 'gift_card' && file.includes('.json')) {
      console.log(`Skipping ${file}...`)
      return;
    }
    
    if (chosenTemplate === 'index' && !file.includes('.json')) {
      console.log(`Skipping ${file}...`)
      return;
    }

    console.log(`Creating ${destPath}${sep}${file}`);
    copyTemplateFile(templateFilesPath, destPath, file, chosenTemplate, 'template');
  });

  readdirSync(destPath).forEach(file => {
    if (chosenTemplate === 'gift_card' && file.includes('.json')) {
      return;
    }

    if (chosenTemplate === 'index' && !file.includes('.json')) {
      return;
    }

    constructTemplate(destPath, file, chosenTemplate, scriptName, templateName);
  })
}