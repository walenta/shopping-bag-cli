import { text, intro, group, cancel } from '@clack/prompts';
import { readdirSync, mkdirSync, copyFileSync, lstatSync, readFileSync, writeFileSync, existsSync } from 'fs';
import { sep } from 'path';
import { __dirname, checkCancel } from './utils.js';

export default async function () {
  const projectName = await text({
    message: 'Please, tell me the name of your gorgeous project...',
    validate: (value) => {
      if (!value?.length) {
        return "Name your project!";
      }

      if (value.length > 25) {
        return 'Project name is too long. (max 25 characters)'
      }

      if (!value.match(/[a-zA-Z0-9\-\_ ]+/g)) {
        return 'Name can only contain letters, numbers, \'-\', \'_\' and spaces'
      }
    }
  });

  checkCancel(projectName);

  const npmProjectName = projectName.replace(/ /g, '-').replace(/-{2,}/g, '-').toLowerCase();
  const projectPath = `${process.cwd()}${sep}${npmProjectName}`;

  if (existsSync(projectPath)) {
    cancel(`Folder with this name already exists (${projectPath}). Goodbye :)`)
    process.exit(0);
  }

  intro('Following answeres will be used ONLY in settings_schema.json (Shopify) and package.json')
  const authorSettings = await group(
    {
      project_author: () => text({message: 'Who is the author of this project?'}),
      support_email: () => text({message: 'Enter support email'}),
      docs_url: () => text({message: 'Enter documentation page url (ex: public GitHub / GitLab repo)'}),
      support_url: () => text({message: 'Enter support page url (ex: GitHub / GitLab issues)'}),
    },
    {
      onCancel: () => checkCancel(null, true)
    }
  );

  authorSettings.project_name = projectName;
  authorSettings.npm_project_name = npmProjectName;
  
  mkdirSync(projectPath, {recursive: true})
  copyFiles(projectPath);

  // author settings
  const packageJsonPath = `${projectPath}${sep}package.json`;
  let packageJsonContents = readFileSync(packageJsonPath, 'utf8');

  const settingsSchemaPath = `${projectPath}${sep}src${sep}config${sep}settings_schema.json`;
  let settingsSchemaContents = readFileSync(settingsSchemaPath, 'utf8');
  
  for (const setting in authorSettings) {
    packageJsonContents = packageJsonContents.replace(new RegExp(`{{${setting}}}`, 'g'), authorSettings[setting] ?? '');
    settingsSchemaContents = settingsSchemaContents.replace(new RegExp(`{{${setting}}}`, 'g'), authorSettings[setting] ?? '');
  }

  writeFileSync(packageJsonPath, packageJsonContents, 'utf8');
  writeFileSync(settingsSchemaPath, settingsSchemaContents, 'utf8');
}

function copyFiles(projectPath, nestedPath) {
  const [srcPath, destPath] = paths(projectPath, nestedPath);
  
  readdirSync(srcPath).forEach(file => {
    const srcFile = `${srcPath}${sep}${file}`;
    if (lstatSync(srcFile).isDirectory()) {
      const destFolder = `${destPath}${sep}${file}`;
      const newNestedPath = `${nestedPath?.length ? `${nestedPath}${sep}` : ''}${file}`;
      mkdirSync(destFolder, {recursive: true});
      copyFiles(projectPath,  newNestedPath);
      return;
    }
    
    if (!existsSync(destPath)) {
      mkdirSync(destPath, {recursive: true});
    }
    
    const destFile = `${destPath}${sep}${file}`;
    copyFileSync(srcFile, destFile);
  });
}

const paths = (projectPath, nestedPath) => [`${__dirname}${sep}assets${sep}init-project${nestedPath?.length ? `${sep}${nestedPath}` : ''}`, `${projectPath}${nestedPath?.length ? `${sep}${nestedPath}` : ''}`];