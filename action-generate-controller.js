import generateTemplate from "./generate-template.js";
import generateSectionSnippet from "./generate-section-snippet.js";

export default async function (generateType) {
  switch (generateType) {
    case 'template':
      generateTemplate();
      break;

    case 'section':
    case 'snippet':
      generateSectionSnippet(generateType);
      break;
  }
}