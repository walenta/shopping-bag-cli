import { Component } from 'shopping-bag-framework';
import styles from './{{file_name}}.scss';

@Component({
  selector: '{{file_name}}',
  importedStyles: [ styles ]
})
export class Snippet{{script_name}} {

}