const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const glob = require('glob');

function getEntries(pattern) {
  const entries = {};

  glob.sync(pattern).forEach((file) => {
    let fileName = file.split('/').at(-1);
    fileNameChunks = fileName.split('.');
    fileName = '';

    for (let i = 0; i < fileNameChunks.length - 1; i++) {
      fileName += fileNameChunks[i];
    }

    entries[fileName] = path.join(__dirname, file);
  });

  return entries;
}

module.exports = env => ({
  mode: env?.mode === 'prod' ? 'production' : 'development',
  entry: {
    'shopping-bag-init': path.resolve(__dirname, 'node_modules/shopping-bag-framework/dist/functions/shopping-bag-init.js'),
    main: path.resolve(__dirname, 'src/assets/ts/main.ts'),
    ...getEntries('src/**/*.js'),
    ...getEntries('src/**/*.ts'),
  },
  output: {
    path: path.resolve(__dirname, env?.mode === 'prod' ? 'dist' : 'build'),
    filename: 'assets/[name].js',
    clean: true
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader",
            options: {
              injectType: "lazyStyleTag",
              insert: function insertIntoTarget(element, options) {
                const parent = options.target || document.head;

                parent.appendChild(element);
              },
            },
          },
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.ts$/,
        exclude: /\.d.ts$/,
        use: [
          'ts-loader'
        ],
      },
      {
        test: /\.(d.ts)$/,
        use: [
          {
            loader: 'null-loader',
          },
        ],
      }
    ]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        // CORE FILES AND ASSETS
        {
          from: 'src/assets/**/*',
          to: 'assets/[name][ext]',
          globOptions: {
            ignore: ['**/*.scss', '**/*.ts'],
          },
          noErrorOnMissing: true
        },
        {
          from: 'src/config/*',
          to: 'config/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/layout/*',
          to: 'layout/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/locales/*',
          to: 'locales/[name][ext]',
          noErrorOnMissing: true
        },
        // SHARED
        {
          from: 'src/shared/sections/**/*.liquid',
          to: 'sections/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/shared/snippets/**/*.liquid',
          to: 'snippets/[name][ext]',
          noErrorOnMissing: true
        },
        // templates
        {
          from: 'src/templates/**/template-*.liquid',
          to: 'sections/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/templates/**/sections/**/*.liquid',
          to: 'sections/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/templates/**/snippets/**/*.liquid',
          to: 'snippets/[name][ext]',
          noErrorOnMissing: true
        },
        {
          from: 'src/templates/**/*.json',
          to: 'templates/[name][ext]',
          noErrorOnMissing: true
        },
      ]
    })
  ]
});