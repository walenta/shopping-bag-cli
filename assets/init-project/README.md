# Shopping bag framework
Framework for creating templates in Shopify online store 2.0 based on AlpineJS

## Features
- More logical (Angular like) file structure
- Data binding into liquid templates (provided by AlpineJS)
- Lazy loaded JS for maximum loading performance
- Code generation with Shopping bag CLI with npm run sb
- preconfigured settings.json for VS Code users
- Preconfigured with:
  - Webpack - everything is bundled into standart shopify structure
  - Shopify theme check
  - Liquid formating
  - ESLint
  - Stylelint

## Recomended extensions (VS Code)
- ESLint by Dbaeumer
- Stylelint by Stylelint
- Liquid by Sissel
- Liquid Languages Support by Neilding
- Shopify Liquid by Shopify
- Auto Rename Tag by Formulahendry
- Pretty TypeScript Errors by yoavbls
- YAML by redhat
- Better Comments by aaron-bond